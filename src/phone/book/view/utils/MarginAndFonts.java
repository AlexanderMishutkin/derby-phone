/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view.utils;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Some popular methods in field of building GUI
 */
public class MarginAndFonts {
    /**
     * Set bold font to component's text
     */
    static public JComponent boldFont(JComponent component) {
        component.setFont(new Font(component.getFont().getName(), Font.BOLD, component.getFont().getSize()));
        return component;
    }

    /**
     * Set margin around component
     */
    static public JComponent setMargin(JComponent component, int top, int left, int bottom, int right) {
        Border border = component.getBorder();
        if (border == null) {
            component.setBorder(new EmptyBorder(top, left, bottom, right));
        } else {
            component.setBorder(new CompoundBorder(new EmptyBorder(top, left, bottom, right), border));
        }
        return component;
    }

    /**
     * Set margin around component
     */
    static public JComponent setMargin(JComponent component, int v, int h) {
        return setMargin(component, v, h, v, h);
    }

    /**
     * Set margin around component
     */
    static public JComponent setMargin(JComponent component, int margin) {
        return setMargin(component, margin, margin);
    }

    /**
     * Creates bold HTML label from common string
     */
    static public String headerToHTML(String header) {
        return "<html><b>" + header.replace(" ", "<br/>") + "</b></html>";
    }

    /**
     * Creates bold HTML label with red star from common string
     */
    static public String appendRedStar(String text) {
        return "<html>" + text + "<span color='red'>*</span></html>";
    }
}
