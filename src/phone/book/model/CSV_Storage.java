/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import com.opencsv.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static phone.book.model.ContactValidateException.NOT_INT_ID_PASSED;


/**
 * Defines some methods to work with CSV files and load <code>Contact</code>'s from them
 */
public class CSV_Storage {
    /**
     * Loads list of <Code>Contact</Code> from given file
     * <p>
     * Validate each contact properties
     *
     * @param src given file
     * @return list of <Code>Contact</Code> from given file
     * @throws IOException              if there are problems with file
     * @throws ContactValidateException if data in file is not appropriating for design model
     */
    public static ArrayList<Contact> load(File src) throws IOException, ContactValidateException {
        ArrayList<Contact> list = new ArrayList<>();
        CSVParser parser = new CSVParser(';');

        try (Reader reader = new FileReader(src)) {
            try (CSVReader csvReader = new CSVReader(reader, 0, parser)) {
                String[] line;
                while ((line = csvReader.readNext()) != null) {
                    Object[] parsed = new Object[line.length];
                    System.arraycopy(line, 0, parsed, 0, line.length);
                    parsed[Contact.ID_INDEX] = Integer.parseInt(line[Contact.ID_INDEX]);
                    list.add(Contact.of(parsed));
                }
            } catch (NumberFormatException e) {
                throw new ContactValidateException(List.of(0), NOT_INT_ID_PASSED);
            }
        }

        return list;
    }

    /**
     * Writes list of <Code>Contact</Code> to the given file
     *
     * @param dest given file
     * @throws IOException if there are problems with file
     */
    public static void save(File dest, List<Contact> contacts) throws IOException {

        try (FileWriter fileWriter = new FileWriter(dest)) {
            try (CSVWriter writer = new CSVWriter(fileWriter, ';')) {
                for (Contact contact : contacts) {
                    String[] s = new String[contact.getFields().length];
                    for (int i = 0; i < contact.getFields().length; i++) {
                        s[i] = contact.getFields()[i].toString();
                    }
                    writer.writeNext(s);
                }
            }
        }
    }
}
