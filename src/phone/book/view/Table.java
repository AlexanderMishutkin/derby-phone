/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view;

import phone.book.model.DataBase;
import phone.book.model.TableModel;
import phone.book.view.dialogs.MiscellaneousDialogs;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * Table (that one on main frame)
 */
public class Table extends JTable {
    public Table(MainWindow app) {
        super(new TableModel());

        // Setting up work of table
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        getColumnModel().setColumnSelectionAllowed(false);
        getTableHeader().setReorderingAllowed(false);
        getTableHeader().setResizingAllowed(false);
        getTableHeader().setPreferredSize(new Dimension(getTableHeader().getPreferredSize().width, 30));
        getTableHeader()
                .setBorder(new CompoundBorder(new LineBorder(new Color(0, 0, 0), 1),
                        getTableHeader().getBorder()));

        // If table creation was unsuccessful
        if (!((TableModel) getModel()).isStorageExists()) {
            if (MiscellaneousDialogs.showDropDBChoice() == 0) {
                DataBase.INSTANCE.reset();
                ((TableModel) getModel()).fireTableDataChanged();
            } else {
                app.exit();
            }
            // MiscellaneousDialogs.showLoadError();
        }
    }
}
