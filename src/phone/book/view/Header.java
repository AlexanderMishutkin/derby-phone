/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view;

import phone.book.controll.FooterController;
import phone.book.controll.MenuController;
import phone.book.controll.SelectionController;
import phone.book.model.TableModel;

import javax.swing.*;
import java.awt.*;

/**
 * Top menu of the main page
 */
public class Header extends JMenuBar {
    private final JMenu file = new JMenu("Файл");
    private final JMenu settings = new JMenu("Настройки");
    private final JMenu about = new JMenu("Справка");

    private final JMenuItem delete = new JMenuItem("Удалить");
    private final JMenuItem edit = new JMenuItem("Редактировать");
    private final JMenuItem add = new JMenuItem("Добавить");
    private final JMenuItem exit = new JMenuItem("Выйти");

    private final JMenuItem importContacts = new JMenuItem("Импортировать контакты");
    private final JMenuItem exportContacts = new JMenuItem("Экспортировать контакты");

    private final JMenuItem aboutItem = new JMenuItem("Справка");

    public Header(MainWindow mainPage, TableModel tableModel,
                  SelectionController selectionController, Table table) {

        createMenuComponents();

        addComponentsListeners(mainPage, tableModel, table);

        // To enable and disable them
        selectionController.addComponent(delete);
        selectionController.addComponent(edit);
    }

    /**
     * Adds listeners to components
     */
    private void addComponentsListeners(MainWindow mainPage, TableModel tableModel, Table table) {
        exit.addActionListener(new MenuController.ExitButtonClickedHandler(mainPage));
        add.addActionListener(new FooterController.AddButtonClickedHandler(tableModel));
        edit.addActionListener(new FooterController.EditButtonClickedHandler(tableModel, table.getSelectionModel()));
        delete.addActionListener(
                new FooterController.DeleteButtonClickedHandler(tableModel, table.getSelectionModel()));


        exportContacts.addActionListener(new MenuController.ExportButtonClickedHandler(tableModel));
        importContacts.addActionListener(new MenuController.ImportButtonClickedHandler(tableModel));

        aboutItem.addActionListener(new MenuController.AboutButtonClickedHandler());
    }

    /**
     * Add components to this panel
     */
    private void createMenuComponents() {
        setLayout(new FlowLayout(FlowLayout.LEFT));

        add(file);
        add(settings);
        add(about);

        file.add(delete);
        file.add(edit);
        file.add(add);
        file.add(exit);

        settings.add(importContacts);
        settings.add(exportContacts);

        about.add(aboutItem);
    }
}
