/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import phone.book.model.ContactValidateException;
import phone.book.model.TableModel;
import phone.book.view.dialogs.AboutDialog;
import phone.book.view.dialogs.MiscellaneousDialogs;
import phone.book.view.MainWindow;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import static phone.book.controll.FrameHierarchyController.getMainFrame;

/**
 * Control work of top menu of application
 */
public class MenuController {
    /**
     * Handles 'exit' button (or menuItem) clicked event
     * <p>
     * Closes the app
     */
    public static class ExitButtonClickedHandler implements ActionListener {
        final MainWindow mainPage;

        /**
         * @param mainPage app to close
         */
        public ExitButtonClickedHandler(MainWindow mainPage) {
            this.mainPage = mainPage;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            mainPage.exit();
        }
    }

    /**
     * Handles 'about' button (or menuItem) clicked event
     */
    public static class AboutButtonClickedHandler implements ActionListener {
        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            AboutDialog.showAboutDialog();
        }
    }

    /**
     * Handles 'export' button (or menuItem) clicked event
     */
    public static class ExportButtonClickedHandler implements ActionListener {
        private final TableModel tm;

        /**
         * @param tableModel to get exported contacts from
         */
        public ExportButtonClickedHandler(TableModel tableModel) {
            tm = tableModel;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser jfc = new JFileChooser();

            if (jfc.showSaveDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) {
                // If file of not csv format selected
                if (!jfc.getSelectedFile().getName().endsWith(".csv")) {
                    int n = MiscellaneousDialogs.showExportFileFormatChoice();
                    if (n == 1) {
                        return;
                    }
                }

                // If filter is used
                int useFilter = 1;
                if (!tm.getFilter().equals("")) {
                    useFilter = MiscellaneousDialogs.showUseTableFilterChoice(tm.getFilter());
                }

                try {
                    // If we rewrites some file by export
                    if (jfc.getSelectedFile().exists()) {
                        if (MiscellaneousDialogs.showRewriteFileChoice(jfc.getSelectedFile()) ==
                                1) {
                            return;
                        }
                    }

                    if (useFilter == 1) {
                        tm.exportTable(jfc.getSelectedFile());
                    } else {
                        tm.exportFilteredTable(jfc.getSelectedFile());
                    }
                } catch (IOException exception) {
                    MiscellaneousDialogs.showExportError(jfc.getSelectedFile());
                }
            }
        }
    }

    /**
     * Handles 'import' button (or menuItem) clicked event
     */
    public static class ImportButtonClickedHandler implements ActionListener {
        final TableModel tm;

        /**
         * @param tableModel to add imported contacts
         */
        public ImportButtonClickedHandler(TableModel tableModel) {
            tm = tableModel;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser jfc = new JFileChooser();
            jfc.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.getAbsolutePath().endsWith(".csv") || f.isDirectory();
                }

                @Override
                public String getDescription() {
                    return "*.csv";
                }
            });

            if (jfc.showOpenDialog(getMainFrame()) == JFileChooser.APPROVE_OPTION) {
                try {
                    tm.importTable(jfc.getSelectedFile());
                } catch (IOException exception) {
                    MiscellaneousDialogs.showImportIOError(jfc.getSelectedFile());
                } catch (ContactValidateException cve) {
                    MiscellaneousDialogs.showImportFormatError(jfc.getSelectedFile());
                }
            }
        }

    }

    /**
     * Handles button in 'about' section events (top secret)
     */
    public static class TotalSelfDestructionHandler implements ActionListener {

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                try {
                    Desktop.getDesktop().browse(new URI("https://www.youtube.com/watch?v=dQw4w9WgXcQ"));
                } catch (Exception ignored) {

                }
            }
        }
    }
}
