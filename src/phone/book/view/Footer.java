/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view;

import phone.book.controll.FooterController;
import phone.book.controll.SelectionController;
import phone.book.model.TableModel;

import javax.swing.*;
import java.awt.*;

/**
 * Footer of main window
 */
public class Footer extends JPanel {
    public Footer(Table table, SelectionController selectionController) {
        // Drawing elements
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        JButton delete = new JButton("Удалить");
        add(delete);
        JButton edit = new JButton("Редактировать");
        add(edit);
        JButton add = new JButton("Добавить");
        add(add);
        JTextField filter = new JTextField();
        add(filter);
        JLabel last = new JLabel(" Сейчас нет фильтра");
        JButton search = new JButton("Поиск");
        last.setPreferredSize(new Dimension(search.getPreferredSize().width * 2, last.getPreferredSize().height));
        filter.setPreferredSize(new Dimension(search.getPreferredSize().width * 2, filter.getPreferredSize().height));
        add(last);
        add(search);

        // Setting up handlers
        filter.addKeyListener(new FooterController.FilterKeyPressedHandler((TableModel) table.getModel(), last));
        search.addActionListener(
                new FooterController.FilterButtonClickedHandler((TableModel) table.getModel(), filter, last));

        add.addActionListener(new FooterController.AddButtonClickedHandler((TableModel) table.getModel()));
        delete.addActionListener(new FooterController.DeleteButtonClickedHandler((TableModel) table.getModel(),
                table.getSelectionModel()));
        edit.addActionListener(new FooterController.EditButtonClickedHandler((TableModel) table.getModel(),
                table.getSelectionModel()));

        selectionController.addComponent(delete);
        selectionController.addComponent(edit);
    }
}
