/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view.dialogs;

import phone.book.model.ContactValidateException;

import javax.swing.*;
import java.io.File;

import static phone.book.controll.FrameHierarchyController.getMainFrame;

/**
 * Shows dialogs for different use
 */
public class MiscellaneousDialogs {
    /**
     * Shows note about export file format
     *
     * @return index of option from possible
     */
    static public int showExportFileFormatChoice() {
        Object[] options = {"Сохранить", "Отмена"};
        return JOptionPane.showOptionDialog(getMainFrame(),
                "Формат экспортируемого файла не .csv." +
                        " Его может быть невозможно прочесть.\nВсе равно сохранить данные?",
                "Плохой формат файла!",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[1]);
    }

    static public int showDropDBChoice() {
        Object[] options = {"Удалить", "Отмена"};
        return JOptionPane.showOptionDialog(getMainFrame(),
                "Возможно удаление старой базы данных поможет...\n" +
                        "Но это приведет к потери всех данных.\n" +
                        "Удалить старую БД?",
                "Удалить базу данных?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[1]);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param filter browser query
     * @return index of option from possible
     */
    static public int showUseTableFilterChoice(String filter) {
        Object[] options = {"Да. Использовать фильтр", "Нет. Экспортировать все данные"};
        return JOptionPane.showOptionDialog(getMainFrame(),
                "К данным сейчас применен фильтр - \"" + filter +
                        "\"\nХотите экспортировать только удовлетворяющие ему данные?",
                "Использовать фильтр?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param file that caused problem or choice
     * @return index of option from possible options
     */
    static public int showRewriteFileChoice(File file) {
        Object[] options = {"Перезаписать файл", "Отмена"};
        return JOptionPane.showOptionDialog(getMainFrame(),
                "Файл " + file.getAbsolutePath() + " уже существует!" +
                        "\nХотите перезаписать старый файл? Его данные не сохраняться!",
                "Перезаписать файл?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param oldName of contact
     * @param newName of contact
     * @return index of option from possible options
     */
    static public int showEditNeedMergeChoice(String oldName, String newName) {
        Object[] options = {"Отмена",
                String.format("Удалить \"%s\" и объединить данные", oldName),
                String.format("Оставить \"%s\" и объединить данные", oldName)};

        return JOptionPane.showOptionDialog(getMainFrame(),
                String.format(
                        "Вы изменили контакт \"%s\" так,\nчто его имя стало \"%s\".\n" +
                                "Но в таблице уже есть человек с таким именем. Как быть?\n" +
                                " - удалить \"%s\" и объединить новый \"%s\" с тем, что уже есть в таблице.\n" +
                                " - оставить \"%s\" и объединить новый \"%s\" с тем, что уже есть в таблице.",
                        oldName, newName,
                        oldName, newName,
                        oldName, newName),
                "Ошибка изменения имени контакта",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param file that caused problem or choice
     */
    static public void showExportError(File file) {
        JOptionPane.showMessageDialog(getMainFrame(),
                "Файл " + file.getAbsolutePath() +
                        "\nне существует или у программы нет прав доступа к нему.\n" +
                        "Попробуйте запустить программу от имени администратора...",
                "Ошибка экспорта контактов!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     */
    static public void showLoadError() {
        JOptionPane.showMessageDialog(getMainFrame(),
                "Файл сохранения состояния таблицы был поврежден, перемещен или доступ к нему закрыт.\n" +
                        "Все внесенные раннее изменения не загрузились...\nПопробуйте запуск в режиме администратора",
                "Ошибка загрузки контактов!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     */
    static public void showBackupError() {
        JOptionPane.showMessageDialog(getMainFrame(),
                "Файл сохранения состояния таблицы был поврежден.\n" +
                        "Все внесенные изменения не сохраняться, нам жаль...",
                "Ошибка сохранения контактов!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param file that caused problem or choice
     */
    public static void showImportIOError(File file) {
        JOptionPane.showMessageDialog(getMainFrame(),
                "Файл " + file.getAbsolutePath() +
                        "\nне существует или у программы нет прав доступа к нему.\n" +
                        "Попробуйте запустить программу от имени администратора...",
                "Ошибка импорта контактов!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param file that caused problem or choice
     */
    public static void showImportFormatError(File file) {
        JOptionPane.showMessageDialog(getMainFrame(),
                "Файл " + file.getAbsolutePath() +
                        "\nсодержит некорректные данные.\n" +
                        "К несчастью, загрузить его не получиться :(",
                "Ошибка импорта контактов!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     *
     * @param exception error in form
     */
    public static void showContactValidationError(ContactValidateException exception) {
        JOptionPane.showMessageDialog(getMainFrame(), exception.getMessage(),
                "Данные контакта содержат ошибки!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     */
    static public void showContactExistsWarning() {
        JOptionPane.showMessageDialog(getMainFrame(),
                "Этот контакт уже есть в таблице.\nОбъединяем информацию...",
                "Контакт уже существует",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Shows dialog - content satisfies description
     */
    public static void showSaveToDataBaseError() {
        JOptionPane.showMessageDialog(getMainFrame(), "Не получается сохранить изменения в базе данных!\n" +
                        "Вероятно вы меняли файлы базы данных во время работы приложения.\n" +
                        "Это плохо, не делайте так!\n" +
                        "Причиной проблемы также может быть вторая программа использующая ДБ -\n" +
                        "убедитесь, что только одно приложение телефонной книги запущено сейчас!",
                "Сохранение в базу данных не удалось!",
                JOptionPane.ERROR_MESSAGE);
    }


    /**
     * Shows dialog - content satisfies description
     */
    public static void showLoadFromDataBaseError() {
        JOptionPane.showMessageDialog(getMainFrame(), "Не получается загрузить контакты из базы данных!\n" +
                        "Вероятно вы меняли файлы базы данных или права доступа к ним.\n" +
                        "Это плохо, не делайте так!\n" +
                        "Причиной проблемы также может быть вторая программа использующая ДБ -\n" +
                        "убедитесь, что только одно приложение телефонной книги запущено сейчас!",
                "Загрузка из базы данных не удалась!",
                JOptionPane.ERROR_MESSAGE);
    }
}
