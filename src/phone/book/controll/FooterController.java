/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import phone.book.model.TableModel;
import phone.book.view.Form;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Control work of bottom menu of application
 */
public class FooterController {
    /**
     * Handles button 'edit' in footer menu clicking
     */
    public static class EditButtonClickedHandler implements ActionListener {
        private final TableModel tableModel;
        private final ListSelectionModel selectionModel;

        /**
         * @param tableModel     table model containing selected contact
         * @param selectionModel selection model with some contact selected
         */
        public EditButtonClickedHandler(TableModel tableModel, ListSelectionModel selectionModel) {
            this.tableModel = tableModel;
            this.selectionModel = selectionModel;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!selectionModel.isSelectionEmpty()) {
                Form.showEditDialog(tableModel,
                        tableModel.getFilteredContacts().get(selectionModel.getMinSelectionIndex()));
            }
        }
    }

    /**
     * Handles button 'add' in footer menu clicking
     */
    public static class AddButtonClickedHandler implements ActionListener {
        private final TableModel tableModel;

        /**
         * @param tableModel in which contact should be added
         */
        public AddButtonClickedHandler(TableModel tableModel) {
            this.tableModel = tableModel;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            Form.showCreateDialog(tableModel);
        }
    }

    /**
     * Reacts on entering 'ENTER' in browser query field
     */
    public static class FilterKeyPressedHandler implements KeyListener {
        final TableModel tableModel;
        private final JLabel label;

        /**
         * @param tableModel to filter
         * @param label      to display filter
         */
        public FilterKeyPressedHandler(TableModel tableModel, JLabel label) {
            this.tableModel = tableModel;
            this.label = label;
        }

        /**
         * Invoked when a key has been typed.
         * See the class description for {@link KeyEvent} for a definition of
         * a key typed event.
         *
         * @param e the event to be processed
         */
        @Override
        public void keyTyped(KeyEvent e) {

        }

        /**
         * Invoked when a key has been pressed.
         * See the class description for {@link KeyEvent} for a definition of
         * a key pressed event.
         *
         * @param e the event to be processed
         */
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                JTextField t = (JTextField) e.getSource();
                tableModel.setFilter(t.getText());
                label.setText(t.getText().equals("") ? " Сейчас нет фильтра" : " Последний: " + t.getText());
                t.setText("");
            }
        }

        /**
         * Invoked when a key has been released.
         * See the class description for {@link KeyEvent} for a definition of
         * a key released event.
         *
         * @param e the event to be processed
         */
        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    /**
     * Handles button 'browse' in footer menu clicking
     */
    public static class FilterButtonClickedHandler implements ActionListener {
        private final TableModel tableModel;
        private final JTextField t;
        private final JLabel label;

        /**
         * @param tableModel to filter
         * @param textField  browser query
         * @param label      to display filter
         */
        public FilterButtonClickedHandler(TableModel tableModel, JTextField textField, JLabel label) {
            this.tableModel = tableModel;
            this.t = textField;
            this.label = label;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            tableModel.setFilter(t.getText());
            label.setText(t.getText().equals("") ? " Сейчас нет фильтра" : " Последний: " + t.getText());
            t.setText("");
        }
    }

    /**
     * Handles button 'delete' in footer menu clicking
     */
    public static class DeleteButtonClickedHandler implements ActionListener {
        private final TableModel tableModel;
        private final ListSelectionModel selectionModel;

        /**
         * @param tableModel     that contains selected contact
         * @param selectionModel that contains contact to delete
         */
        public DeleteButtonClickedHandler(TableModel tableModel, ListSelectionModel selectionModel) {
            this.tableModel = tableModel;
            this.selectionModel = selectionModel;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            if (selectionModel.getSelectedItemsCount() != 0) {
                tableModel.removeContact(tableModel.getFilteredContacts().get(selectionModel.getSelectedIndices()[0]));
            }
        }
    }
}
