/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Controls components that should be active if some contact is selected
 */
public class SelectionController {
    private final ListSelectionModel selectionModel;
    final ArrayList<JComponent> components;

    /**
     * @param selectionModel to bind with components
     */
    public SelectionController(ListSelectionModel selectionModel) {
        this.selectionModel = selectionModel;
        components = new ArrayList<>();
        selectionModel.addListSelectionListener(e -> {
            if (selectionModel.getSelectedItemsCount() <= 0) {
                components.forEach((JComponent c) -> c.setEnabled(false));
            }
            if (selectionModel.getSelectedItemsCount() > 0) {
                components.forEach((JComponent c) -> c.setEnabled(true));
            }
        });
    }

    /**
     * @param component add one more component bind to selection
     */
    public void addComponent(JComponent component) {
        components.add(component);
        if (selectionModel.getSelectedItemsCount() <= 0) {
            component.setEnabled(false);
        }
        if (selectionModel.getSelectedItemsCount() > 0) {
            component.setEnabled(true);
        }
    }
}
