/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Class representing a phone book row - information about single person.
 */
public class Contact {
    private final String surname;
    static public final int SURNAME_INDEX = 1;

    private final String name;
    static public final int NAME_INDEX = 2;

    private final String patronymic;
    static public final int PATRONYMIC_INDEX = 3;

    private final String mobilePhone;
    static public final int MOBILE_PHONE_INDEX = 4;

    private final String homePhone;
    static public final int HOME_PHONE_INDEX = 5;

    private final String address;
    static public final int ADDRESS_INDEX = 6;

    private final Date birthday;
    static public final int BIRTHDAY_INDEX = 7;

    private final String comment;
    static public final int COMMENT_INDEX = 8;

    private final int id;
    static public final int ID_INDEX = 0;

    Contact(int id, String name, String surname, String patronymic, String mobilePhone, String homePhone, String address,
            String comment) {
        this(id, name, surname, patronymic, mobilePhone, homePhone, address, null, comment);
    }

    Contact(int id, String name, String surname, String patronymic, String mobilePhone, String homePhone, String address,
            Date birthday, String comment) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.mobilePhone = mobilePhone;
        this.homePhone = homePhone;
        this.address = address;
        this.birthday = birthday;
        this.comment = comment;
        this.id = id;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public int getId() {
        return id;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getName() {
        return name;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getPatronymic() {
        return patronymic;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getAddress() {
        return address;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getComment() {
        return comment;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * Represents all fields of contact as array of <code>String</code> converted to <code>Object</code>
     * for usage in <code>TableModel</code>
     * <p>
     * Order of fields is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     *
     * @return fields converted to <code>String</code>
     */
    public Object[] getFields() {
        return new Object[]{
                id,
                surname,
                name,
                patronymic,
                mobilePhone,
                homePhone,
                address,
                birthday != null ? FORMATTER.format(birthday) : "",
                comment};
    }

    /**
     * Represents names of all fields of contact as array of <code>String</code> (in Russian language)
     * for usage in <code>TableModel</code>
     * <p>
     * Order of fields is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     *
     * @return field's names converted to <code>String</code>
     */
    static public String[] getFieldsNames() {
        return new String[]{
                "№",
                "Фамилия",
                "Имя",
                "Отчество",
                "Мобильный телефон",
                "Домашний телефон",
                "Адрес",
                "День рождения",
                "Комментарий"};
    }

    /**
     * Format date to the <code>dd.MM.yyyy</code> pattern
     */
    static public final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * Builds <code>Contact</code> from array of it's fields' values.
     * <p>
     * Values should be convertible to <code>String</code> their order is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     * <p>
     * Validates each field value as it is described in design
     *
     * @param array fields' values
     * @return new <code>Contact</code>
     * @throws ContactValidateException if some field has not appropriating data
     */
    public static Contact of(Object[] array) throws ContactValidateException {
        ArrayList<Integer> errors = new ArrayList<>();
        StringBuilder msg = new StringBuilder();

        if (array.length < getFieldsNames().length) {
            throw new ContactValidateException(errors, " ∙ " + ContactValidateException.NOT_ENOUGH_FIELDS);
        }

        if (!(array[ID_INDEX] instanceof Integer)) {
            errors.add(ID_INDEX);
            msg.append(" ∙ " + ContactValidateException.NOT_INT_ID_PASSED);
            msg.append(System.lineSeparator());
        }
        for (int i = ID_INDEX + 1; i < array.length; i++) {
            if (!(array[i] instanceof String)) {
                errors.add(i);
                msg.append(" ∙ " + ContactValidateException.NOT_STRING_ELEMENT_PASSED);
                msg.append(System.lineSeparator());
            }
        }

        if (!errors.isEmpty()) {
            throw new ContactValidateException(errors, msg.toString());
        }

        for (int i = ID_INDEX; i < array.length; i++) {
            if (array[i].toString().replace(" ", "").contains("</") ||
                    array[i].toString().replace(" ", "").contains(">")) {
                errors.add(i);
                msg.append(" ∙ " + ContactValidateException.HTML_INJECTION);
                msg.append(System.lineSeparator());
            }
        }

        String surname = (String) array[SURNAME_INDEX];
        String name = (String) array[NAME_INDEX];

        if (surname.isBlank()) {
            errors.add(SURNAME_INDEX);
            msg.append(" ∙ " + ContactValidateException.NECESSARY_FIELD_IS_EMPTY);
            msg.append(System.lineSeparator());
        }
        if (name.isBlank()) {
            errors.add(NAME_INDEX);
            msg.append(" ∙ " + ContactValidateException.NECESSARY_FIELD_IS_EMPTY);
            msg.append(System.lineSeparator());
        }

        int id = (Integer) array[ID_INDEX];
        String patronymic = (String) array[PATRONYMIC_INDEX];
        String mobilePhone = (String) array[MOBILE_PHONE_INDEX];
        String homePhone = (String) array[HOME_PHONE_INDEX];
        String address = (String) array[ADDRESS_INDEX];
        String comment = (String) array[COMMENT_INDEX];
        Date birthday;

        if (homePhone.isBlank() && mobilePhone.isBlank()) {
            errors.add(MOBILE_PHONE_INDEX);
            errors.add(HOME_PHONE_INDEX);
            msg.append(" ∙ " + ContactValidateException.NO_PHONE_GIVEN);
            msg.append(System.lineSeparator());
        }

        if (!mobilePhone.matches("^[0-9]*$")) {
            errors.add(MOBILE_PHONE_INDEX);
            msg.append(" ∙ " + ContactValidateException.LETTER_IN_PHONE);
            msg.append(System.lineSeparator());
        }

        if (!homePhone.matches("^[0-9]*$")) {
            errors.add(HOME_PHONE_INDEX);
            msg.append(" ∙ " + ContactValidateException.LETTER_IN_PHONE);
            msg.append(System.lineSeparator());
        }

        if (array[BIRTHDAY_INDEX].toString().isBlank()) {
            if (!errors.isEmpty()) {
                throw new ContactValidateException(errors, msg.toString());
            }

            return new Contact(id, name, surname, patronymic, mobilePhone, homePhone, address, comment);
        }

        try {
            birthday = FORMATTER.parse((String) array[BIRTHDAY_INDEX]);
            if (birthday.after(new Date())) {
                errors.add(BIRTHDAY_INDEX);
                msg.append(" ∙ " + ContactValidateException.BORN_IN_FUTURE);
                msg.append(System.lineSeparator());
            }
        } catch (DateTimeException | ParseException dte) {
            throw new ContactValidateException(List.of(BIRTHDAY_INDEX), " ∙ " + ContactValidateException.BAD_DATE_FORMAT);
        }

        if (!errors.isEmpty()) {
            throw new ContactValidateException(errors, msg.toString());
        }

        return new Contact(id, name, surname, patronymic, mobilePhone, homePhone, address, birthday, comment);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contact contact = (Contact) o;
        return getName().equals(contact.getName()) &&
                getSurname().equals(contact.getSurname()) &&
                getPatronymic().equals(contact.getPatronymic());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getPatronymic());
    }

    /**
     * Gives fields that are different between two contacts
     * <p>
     * Order ov fields is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     *
     * @param contact contact to compare
     * @return list of different field indices
     */
    public ArrayList<Integer> diff(Contact contact) {
        ArrayList<Integer> d = new ArrayList<>();

        for (int i = 1; i < contact.getFields().length; i++) {
            if (!contact.getFields()[i].equals(getFields()[i])) {
                d.add(i);
            }
        }
        return d;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (Object s :
                getFields()) {
            out.append(s);
            out.append("\t");
        }
        return out.toString();
    }
}
