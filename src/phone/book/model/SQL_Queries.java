/*
 * <a href="mailto:apmishutkin@edu.hse.ru"> Mishutkin Alexander </a>
 */

package phone.book.model;

/**
 * Constant SQL commands to ease editing table <code>Contacts</code> in class <code>DataBase</code>
 */
public class SQL_Queries {
    /**
     * Creates table <code>Contacts</code>
     */
    static final public String CREATE =
            "CREATE TABLE Contacts("
            + "id           INTEGER           NOT NULL  GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
            + "surname      VARCHAR(255)      NOT NULL,"
            + "name         VARCHAR(255)      NOT NULL,"
            + "patronymic   VARCHAR(255)      NOT NULL,"
            + "mobile_phone VARCHAR(255)      NOT NULL,"
            + "home_phone   VARCHAR(255)      NOT NULL,"
            + "address      VARCHAR(4095)     NOT NULL,"
            + "birthday     DATE                      ,"
            + "comment      LONG VARCHAR      NOT NULL)";

    /**
     * Adds contact to the table
     */
    static final public String ADD =
            "INSERT INTO Contacts(surname, name, patronymic, mobile_phone, home_phone, address, birthday, comment)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Alter contact in the table
     */
    static final public String EDIT =
            "UPDATE       Contacts " +
                    "SET " +
                    "surname      =         ?," +
                    "name         =         ?," +
                    "patronymic   =         ?," +
                    "mobile_phone =         ?," +
                    "home_phone   =         ?," +
                    "address      =         ?," +
                    "birthday     =         ?," +
                    "comment      =         ?" +
                    "WHERE " +
                    "id           =         ?";

    /**
     * Delete contact from the table
     */
    static final public String DELETE =
            "DELETE FROM Contacts WHERE " +
                    "ID = ?";

    /**
     * Search for contacts with given substring in the name in the table
     */
    public static final String FILTER =
            "SELECT * FROM Contacts WHERE " +
                    "(surname       LIKE    '%'|| ? || '%') OR " +
                    "(name          LIKE    '%'|| ? || '%') OR " +
                    "(patronymic    LIKE    '%'|| ? || '%')";

    /**
     * Search for contact by its name
     */
    public static final String FIND =
            "SELECT * FROM Contacts WHERE " +
                    "surname=(?)    AND " +
                    "name=(?)       AND " +
                    "patronymic=(?)";

    /**
     * Remove all contacts from the table
     */
    public static final String CLEAR =
            "DELETE FROM Contacts";
}
