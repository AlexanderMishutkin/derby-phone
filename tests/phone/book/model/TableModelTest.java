/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import org.apache.derby.iapi.services.io.FileUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests correct work with data by <code>TableModel</code>. Do not care about GUI
 * <p>
 * Most of tests are foobar, but changing strategies in merge test and work with files is not trivial
 */
class TableModelTest {
    TableModel table;
    static final File file = new File("phone_book.csv");
    static final File export = new File("export_target.csv");

    @BeforeEach
    void setUp() {
        FileUtil.removeDirectory(new File("Test"));
        DataBase.INSTANCE.DB_NAME = "Test";
        DataBase.INSTANCE.connectDB();

        try {
            if (!DataBase.INSTANCE.isNotConnected()) {
                DataBase.INSTANCE.connection.createStatement().execute("DROP TABLE Contacts");
                System.out.println("Old table removed");
                DataBase.INSTANCE.disconnect();
                DataBase.INSTANCE.connectDB();
            } else {
                System.out.println("There was no table");
            }
        } catch (SQLException exception) {
            fail(exception);
        }

        try {
            assertFalse(DataBase.INSTANCE.isNotConnected());
            DataBase.INSTANCE.addContact(new Contact(1, "12m34", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(2, "12s34", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(3, "12h34", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(4, "12m34", "k11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(5, "12m345", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.getFilteredContacts("").forEach(System.out::println);
        } catch (SQLException | ContactValidateException exception) {
            fail(exception);
        }

        TableModel.setSilent(true);
        table = new TableModel();
    }

    @AfterEach
    void tearDown() {
        DataBase.INSTANCE.disconnect();
        TableModel.setSilent(false);
    }

    @AfterAll
    static void afterAll() {
        new File("Test").deleteOnExit();
    }

    /**
     * Boyscout rule)
     */
    @AfterAll
    static void deleteExportStorage() {
        try {
            boolean b = false;
            if (export.exists()) {
                b = !export.delete();
            }
            if (b) {
                fail("Storage is blocked!");
            }
        } catch (Exception ignored) {

        }
    }

    @Test
    void save() {
        ArrayList<Contact> loaded = new ArrayList<>();

        try {
            table.save();
        } catch (IOException e) {
            fail("Export failed!");
        }

        try {
            loaded = CSV_Storage.load(file);
        } catch (IOException | ContactValidateException e) {
            fail(e);
        }

        assertArrayEquals(table.getFilteredContacts().toArray(), loaded.toArray());
    }

    @Test
    void export() {
        ArrayList<Contact> loaded = new ArrayList<>();

        try {
            table.exportTable(export);
        } catch (IOException e) {
            fail("Export failed!");
        }

        try {
            loaded = CSV_Storage.load(export);
        } catch (IOException | ContactValidateException e) {
            fail(e);
        }

        assertArrayEquals(table.getFilteredContacts().toArray(), loaded.toArray());
    }

    @Test
    void exportFiltered() {
        ArrayList<Contact> loaded = new ArrayList<>();

        Contact two = table.getFilteredContacts().get(1);
        table.setFilter("12s34");
        try {
            table.exportFilteredTable(export);
        } catch (IOException e) {
            fail("Export failed!");
        }

        try {
            loaded = CSV_Storage.load(export);
        } catch (IOException | ContactValidateException e) {
            fail(e);
        }

        assertArrayEquals(table.getFilteredContacts().toArray(), loaded.toArray());
        assertArrayEquals(new Contact[]{two}, loaded.toArray());
    }

    @Test
    void replaceContact() {
        try {
            table.replaceContact(Contact.of(new Object[]{1, "1", "1", "1", "", "2", "", "", ""}));
            assertArrayEquals(new Object[]{1, "1", "1", "1", "", "2", "", "", ""},
                    table.getFilteredContacts().get(0).getFields());
        } catch (ContactValidateException contactValidateException) {
            fail(contactValidateException);
        }
    }

    @Test
    void removeAndContains() {
        Contact c = table.getFilteredContacts().get(0);
        try {
            assertTrue(table.contains(c));
        } catch (ContactValidateException | SQLException e) {
            fail(e);
        }
        table.removeContact(c);
        try {
            assertFalse(table.contains(c));
        } catch (ContactValidateException | SQLException e) {
            fail(e);
        }
        assertNotEquals(c, table.getFilteredContacts().get(0));
    }

    @Test
    void getRowCount() {
        assertEquals(5, table.getRowCount());
    }

    @Test
    void getColumnCount() {
        assertEquals(Contact.getFieldsNames().length, table.getColumnCount());
    }

    @Test
    void getValueAt() {
        assertEquals("10.05.2021", table.getValueAt(0, 7));
        assertEquals("a11", table.getValueAt(1, 1));
        assertEquals("12h34", table.getValueAt(2, 2));
    }

    @Test
    void getColumnName() {
        for (int i = 0; i < Contact.getFieldsNames().length; i++) {
            // Column name is HTML. We need extract raw text from it
            assertEquals(Contact.getFieldsNames()[i], table.getColumnName(i)
                    .replaceAll("<[a-z/]+>", " ")
                    .replaceAll(" +", " ").trim());
        }
    }

    @Test
    void setFilter() {
        Contact two = table.getFilteredContacts().get(1);
        table.setFilter("12s34");
        assertArrayEquals(new Contact[]{two}, table.getFilteredContacts().toArray());
    }

    @Test
    void getFilter() {
        assertEquals("", table.getFilter());
        table.setFilter("KEK LOL");
        assertEquals("KEK LOL", table.getFilter());
    }

    /**
     * Uses special merge strategy to test merge without user choice
     */
    @Test
    void mergeContacts() {
        table.setMergeStrategy((head, feature) -> MergeStrategy.ALWAYS_LEAVE_OLD);
        table.mergeContacts(List.of(new Contact(2, "12s34", "a11", "22p", "7", "0", "spb", new Date(), "kek")));
        assertEquals("msc", table.getFilteredContacts().get(1).getAddress());

        table.setMergeStrategy((head, feature) -> MergeStrategy.ALWAYS_TAKE_NEW);
        table.mergeContacts(List.of(new Contact(2, "12s34", "a11", "22p", "7", "0", "spb", new Date(), "kek")));
        assertEquals("spb", table.getFilteredContacts().get(1).getAddress());
    }

    @Test
    void extreme() {
        try {
            DataBase.INSTANCE.connectDB();
            if (!DataBase.INSTANCE.isNotConnected()) {
                DataBase.INSTANCE.connection.createStatement().execute("DROP TABLE Contacts");
            }
        } catch (SQLException exception) {
            fail(exception);
        }
        assertDoesNotThrow(() -> {
            table = new TableModel();
        });
        assertFalse(table.isStorageExists());
        assertDoesNotThrow(() -> table.fireTableDataChanged());
        assertDoesNotThrow(() -> {
            try {
                table.save();
            } catch (IOException ignored) {

            }
        });
        assertDoesNotThrow(() -> {
            try {
                table.exportTable(export);
            } catch (IOException ignored) {

            }
        });
        assertDoesNotThrow(() -> {
            try {
                table.exportFilteredTable(export);
            } catch (IOException ignored) {

            }
        });
        assertDoesNotThrow(() -> {
            try {
                table.exportFilteredTable(export);
            } catch (IOException ignored) {

            }
        });
    }

    @Test
    void addContacts() {
        table.addContacts(List.of(new Contact(2, "2", "1", "1", "1", "0", "1", new Date(), "1")));
        assertEquals("2", table.getFilteredContacts().get(5).getName());
        assertEquals(6, table.getFilteredContacts().get(5).getId());

        table.setMergeStrategy((head, feature) -> MergeStrategy.ALWAYS_LEAVE_OLD);
        table.addContacts(List.of(new Contact(2, "12s34", "a11", "22p", "7", "0", "spb", new Date(), "kek")));
        assertEquals("msc", table.getFilteredContacts().get(1).getAddress());

        table.setMergeStrategy((head, feature) -> MergeStrategy.ALWAYS_TAKE_NEW);
        table.addContacts(List.of(new Contact(2, "12s34", "a11", "22p", "7", "0", "spb", new Date(), "kek")));
        assertEquals("spb", table.getFilteredContacts().get(1).getAddress());
    }
}