/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import phone.book.model.Contact;
import phone.book.model.ContactValidateException;
import phone.book.model.TableModel;
import phone.book.view.Form;
import phone.book.view.dialogs.MiscellaneousDialogs;

import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.List;

/**
 * Controls events in edit/add form
 */
public class FormController {
    /**
     * Controls form closing (just close dialog)
     */
    public static class CloseHandler implements ActionListener {
        final Form form;

        /**
         * @param form to close
         */
        public CloseHandler(Form form) {
            this.form = form;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            form.exit();
        }
    }

    /**
     * Controls button 'create' click events
     */
    public static class CreateHandler implements ActionListener {
        private final Form form;
        private final TableModel tableModel;

        /**
         * @param form       with data about new contact
         * @param tableModel to add contact in
         */
        public CreateHandler(Form form, TableModel tableModel) {
            this.form = form;
            this.tableModel = tableModel;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            Contact contact;
            try {
                contact = Contact.of(form.getUserInput());
            } catch (ContactValidateException contactValidateException) {
                form.highlightWrongFields(contactValidateException);
                MiscellaneousDialogs.showContactValidationError(contactValidateException);
                return;
            }


            try {
                if (tableModel.contains(contact)) {
                    MiscellaneousDialogs.showContactExistsWarning();
                    tableModel.mergeContacts(List.of(contact));
                    form.exit();
                    return;
                }
            } catch (ContactValidateException | SQLException contactValidateException) {
                MiscellaneousDialogs.showLoadFromDataBaseError();
                return;
            }
            tableModel.addContacts(List.of(contact));
            form.exit();
        }
    }

    /**
     * Controls button 'edit' click events
     */
    public static class EditHandler implements ActionListener {
        private final Form form;
        private final TableModel tableModel;
        private final Contact contact;

        /**
         * @param form       with edited contact data
         * @param tableModel containing edible contact
         * @param contact    old contact
         */
        public EditHandler(Form form, TableModel tableModel, Contact contact) {
            this.form = form;
            this.tableModel = tableModel;
            this.contact = contact;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            Contact newContact;
            try {
                newContact = Contact.of(form.getUserInput());
            } catch (ContactValidateException contactValidateException) {
                form.highlightWrongFields(contactValidateException);
                MiscellaneousDialogs.showContactValidationError(contactValidateException);
                return;
            }

            // If contact after editing become copy of another one
            try {
                if (!contact.equals(newContact) && tableModel.contains(newContact)) {
                    int n = MiscellaneousDialogs.showEditNeedMergeChoice(
                            contact.getName() + " " + contact.getSurname() + " " + contact.getPatronymic(),
                            newContact.getName() + " " + newContact.getSurname() + " " + newContact.getPatronymic());

                    if (n == 1) {
                        tableModel.removeContact(contact);
                        tableModel.mergeContacts(List.of(newContact));
                        form.exit();
                    }
                    if (n == 2) {
                        tableModel.mergeContacts(List.of(newContact));
                        form.exit();
                    }
                    return;
                }
            } catch (ContactValidateException | SQLException contactValidateException) {
                MiscellaneousDialogs.showLoadFromDataBaseError();
                return;
            }

            tableModel.replaceContact(newContact);
            form.exit();
        }
    }

    /**
     * Controls phone number input (deletes non number symbols)
     */
    public static class NumberKeysHandler implements KeyListener {
        private final JTextComponent field;

        /**
         * @param field that must contain only numbers (and +,(,), ,-)
         */
        public NumberKeysHandler(JTextComponent field) {
            this.field = field;
        }

        /**
         * Invoked when a key has been typed.
         * See the class description for {@link KeyEvent} for a definition of
         * a key typed event.
         *
         * @param e the event to be processed
         */
        @Override
        public void keyTyped(KeyEvent e) {
        }

        /**
         * Invoked when a key has been pressed.
         * See the class description for {@link KeyEvent} for a definition of
         * a key pressed event.
         *
         * @param e the event to be processed
         */
        @Override
        public void keyPressed(KeyEvent e) {

        }

        /**
         * Invoked when a key has been released.
         * See the class description for {@link KeyEvent} for a definition of
         * a key released event.
         *
         * @param e the event to be processed
         */
        @Override
        public void keyReleased(KeyEvent e) {
            if (field.getText().matches(".* {2}.*") || field.getText().matches(".*[^0-9+()\\- ].*")) {
                field.setText(field.getText()
                        .replaceAll(" [ ]+", " ")
                        .replaceAll("[^0-9+()\\- ]", ""));
            }
        }
    }
}
