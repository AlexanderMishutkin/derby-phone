/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import phone.book.model.DataBase;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Control main window and it's own events
 */
public class AppController {
    /**
     * Control closing of main event
     */
    public static class DisconnectDataBaseAdapter extends WindowAdapter {
        public DisconnectDataBaseAdapter() {
        }

        /**
         * Invoked when a window is in the process of being closed.
         * The close operation can be overridden at this point.
         *
         * @param e WindowEvent
         */
        @Override
        public void windowClosing(WindowEvent e) {
            DataBase.INSTANCE.disconnect();
            super.windowClosing(e);
        }

    }
}
