/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view;

import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import phone.book.controll.FormController;
import phone.book.model.Contact;
import phone.book.model.ContactValidateException;
import phone.book.model.TableModel;
import phone.book.view.utils.MarginAndFonts;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.StrokeBorder;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.*;

/**
 * Contact edit and create form
 */
public class Form extends JDialog {
    private final JTextArea note = new JTextArea();
    private final JTextComponent[] fields = new JTextComponent[Contact.getFieldsNames().length];
    private final int[] phoneNumberFieldsIndices = new int[]{Contact.MOBILE_PHONE_INDEX, Contact.HOME_PHONE_INDEX};
    private final int[] nameFieldsIndices = new int[]{Contact.SURNAME_INDEX, Contact.NAME_INDEX};
    private final Border defaultBorder = new JTextField().getBorder();
    final JButton save;
    final JButton cancel;
    private final int datePickerIndex = Contact.BIRTHDAY_INDEX;
    private JDatePickerImpl datePicker;
    private int id = -1;

    /**
     * Highlight wrong fields in form
     *
     * @param contactValidateException from Contact.of(...)
     */
    public void highlightWrongFields(ContactValidateException contactValidateException) {
        if (contactValidateException.getIndices().contains(datePickerIndex)) {
            datePicker.setBorder(new StrokeBorder(new BasicStroke(), new Color(255, 0, 0)));
            datePicker.repaint();
        }

        for (int i = 0; i < fields.length; i++) {
            if (i == datePickerIndex) {
                continue;
            }

            if (contactValidateException.getIndices().contains(i)) {
                fields[i].setBorder(new StrokeBorder(new BasicStroke(), new Color(255, 0, 0)));
            } else {
                fields[i].setBorder(defaultBorder);
            }

            fields[i].repaint();
        }
    }

    /**
     * Creates grid with input fields
     */
    private void generateFields() {
        JPanel contentPane = (JPanel) getContentPane();
        JPanel grid = (JPanel) MarginAndFonts
                .setMargin(new JPanel(new GridLayout(Contact.getFieldsNames().length - 1, 2, 10, 5)), 5, 0);

        for (int i = 0; i < Contact.getFieldsNames().length - 1; i++) {
            String label = Contact.getFieldsNames()[i];

            // Add red star to name fields
            int finalI = i;
            if (Arrays.stream(nameFieldsIndices).anyMatch((j) -> j == finalI)) {
                label = MarginAndFonts.appendRedStar(label);
            }
            grid.add(new JLabel(label));

            // Date field is not JTextField
            if (i == datePickerIndex) {
                datePicker = createDatePicker();
                grid.add(datePicker);
            } else {
                fields[i] = new JTextField();
                fields[i].setToolTipText(Contact.getFieldsNames()[i]);
                grid.add(fields[i]);

                if (i == Contact.ID_INDEX) {
                    if (id == -1) {
                        fields[i].setText("(устанавливается базой контактов)");
                    } else {
                        fields[i].setText(((Integer)id).toString());
                    }
                    fields[i].setEnabled(false);
                }
            }
        }

        for (int phoneNumberIndex : phoneNumberFieldsIndices) {
            // Control that phone number consists of numbers
            fields[phoneNumberIndex]
                    .addKeyListener(new FormController.NumberKeysHandler(fields[phoneNumberIndex]));
        }

        fields[Contact.getFieldsNames().length - 1] = note;
        contentPane.add(grid, BorderLayout.NORTH);
    }

    private Form() {
        super(new JFrame(), true);

        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setLayout(new BorderLayout());
        MarginAndFonts.setMargin(contentPane, 5, 10);

        JPanel footer = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        save = new JButton("Сохранить");
        cancel = new JButton("Отмена");

        cancel.addActionListener(new FormController.CloseHandler(this));

        footer.add(cancel);
        footer.add(save);

        note.setPreferredSize(new Dimension(300, 70));
        setMinimumSize(new Dimension(310, 410));

        contentPane.add(note, BorderLayout.CENTER);
        contentPane.add(footer, BorderLayout.SOUTH);

        generateFields();
        datePicker.setToolTipText(Contact.getFieldsNames()[datePickerIndex]);
        pack();
    }

    /**
     * Shows dialog allowing users to create new contacts
     * <p>
     * Provides contacts validation
     *
     * @param tableModel table model to add contacts in
     */
    static public void showCreateDialog(TableModel tableModel) {
        Form form = new Form();
        form.setTitle("Создание контакта");

        form.save.addActionListener(new FormController.CreateHandler(form, tableModel));

        form.setVisible(true);
    }

    /**
     * Shows dialog allowing users to change given contact
     *
     * @param tableModel that has given contact
     * @param contact    to change
     */
    static public void showEditDialog(TableModel tableModel, Contact contact) {
        Form form = new Form();
        form.setTitle("Редактирование контакта");

        // Putting existing data in form fields
        for (int i = 0; i < form.fields.length; i++) {
            if (i != form.datePickerIndex) {
                form.fields[i].setText(contact.getFields()[i].toString());
            } else {
                if (contact.getBirthday() != null) {
                    Calendar day = new GregorianCalendar();
                    day.setTime(contact.getBirthday());
                    form.datePicker.getModel()
                            .setDate(day.get(Calendar.YEAR), day.get(Calendar.MONTH), day.get(Calendar.DAY_OF_MONTH));
                    form.datePicker.getModel().setSelected(true);
                }
            }
        }

        form.save.addActionListener(new FormController.EditHandler(form, tableModel, contact));
        form.id = contact.getId();

        form.setVisible(true);
    }

    public void exit() {
        processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * @return content of input fields as strings
     */
    public Object[] getUserInput() {
        Object[] input = new Object[Contact.getFieldsNames().length];
        for (int i = 0; i < input.length; i++) {
            if (i == Contact.ID_INDEX) {
                input[i] = id;
                continue;
            }

            if (i == datePickerIndex) {
                if (datePicker.getModel().isSelected()) {
                    Date date = (Date) datePicker.getModel().getValue();
                    input[i] = date == null ? "" : Contact.FORMATTER.format(date);
                } else {
                    input[i] = "";
                }
                continue;
            }

            int finalI = i;
            if (Arrays.stream(phoneNumberFieldsIndices).anyMatch((j) -> j == finalI)) {
                fields[i].setText(
                        fields[i].getText()
                                .replace("+", "")
                                .replace("(", "")
                                .replace(")", "")
                                .replace("-", "")
                                .replace(" ", ""));
            }

            input[i] = fields[i].getText();
        }
        return input;
    }

    /**
     * @return creates new <code>JDatePickerImpl</code>
     */
    public JDatePickerImpl createDatePicker() {
        UtilDateModel dateModel = new UtilDateModel();
        JDatePanelImpl datePanel = new JDatePanelImpl(dateModel, new Properties());
        return new JDatePickerImpl(datePanel, new DateComponentFormatter());
    }
}
