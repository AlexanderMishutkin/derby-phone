/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import phone.book.view.dialogs.MergeDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controls work of merge dialog
 */
public class MergeDialogController {
    /**
     * Handles the selection of 'take old' option
     * <p>
     * Just change <code>mergeDialog.choice</code>
     */
    public static class TakeOldHandler implements ActionListener {
        final MergeDialog mergeDialog;

        /**
         * @param mergeDialog dialog where the choice happens
         */
        public TakeOldHandler(MergeDialog mergeDialog) {
            this.mergeDialog = mergeDialog;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            mergeDialog.setChoice(0);

            if (mergeDialog.getCheckBox().getModel().isSelected()) {
                mergeDialog.setChoice(2);
            }

            mergeDialog.exit();
        }
    }

    /**
     * Handles the selection of 'take new' option
     * <p>
     * Just change <code>mergeDialog.choice</code>
     */
    public static class TakeNewHandler implements ActionListener {
        final MergeDialog mergeDialog;

        /**
         * @param mergeDialog dialog where the choice happens
         */
        public TakeNewHandler(MergeDialog mergeDialog) {
            this.mergeDialog = mergeDialog;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            mergeDialog.setChoice(1);

            if (mergeDialog.getCheckBox().getModel().isSelected()) {
                mergeDialog.setChoice(3);
            }

            mergeDialog.exit();
        }
    }
}
