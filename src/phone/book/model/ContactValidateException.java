/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import java.util.List;

/**
 * Special exception to throw in <code>Contact</code> builder
 */
public class ContactValidateException extends Exception {
    final private List<Integer> indices;

    /**
     * @param indices index of first wrong field
     * @param message what is wrong (use constants from this class)
     */
    public ContactValidateException(List<Integer> indices, String message) {
        super(message.contains("\n") ? MANY_ERRORS + System.lineSeparator() + message : message);
        this.indices = indices;
    }

    /**
     * @return index of first wrong field
     */
    public List<Integer> getIndices() {
        return indices;
    }

    public static final String MANY_ERRORS = "Было найдено несколько ошибок в контакте:";
    public static final String HTML_INJECTION = "В поле встетились нетипичные символы: \"<html/>\", " +
            "что ведет к подозрению на html-инъекцию. Совершать киберприступления плохо!";
    static final public String NECESSARY_FIELD_IS_EMPTY = "Не возможно сохранить контакт без фамилии и имени!";
    static final public String NO_PHONE_GIVEN = "Укажите хотя бы один телефонный номер контакта!";
    static final public String BAD_DATE_FORMAT = "Дата рождения указана в неверном формате (dd.MM.yyyy)";
    static final public String BORN_IN_FUTURE = "Человек не может обладать телефоном если еще не рожден...";
    static final public String NOT_STRING_ELEMENT_PASSED = "Значение поля равно NULL или не является строкой!";
    static final public String NOT_ENOUGH_FIELDS =
            "для создание контакта нужно " + Contact.getFieldsNames().length + " полей!";
    static final public String LETTER_IN_PHONE =
            "В телефонном номере (как понимают его создатели приложения) могут быть только цифры";
    static final public String NOT_INT_ID_PASSED =
            "ID контакта - целое число";
}
