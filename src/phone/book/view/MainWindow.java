/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view;

import phone.book.controll.SelectionController;
import phone.book.controll.AppController;
import phone.book.controll.FrameHierarchyController;
import phone.book.model.TableModel;
import phone.book.view.utils.Localization;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

/**
 * Main window (that one with table)
 */
public class MainWindow extends JFrame {
    private final JPanel contentPane;

    public MainWindow() {
        super("Телефонная книга");
        Localization.localize();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); // Default look and feel is foobar :(
        } catch (Exception ignored) {

        }

        contentPane = (JPanel) getContentPane();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(new AppController.DisconnectDataBaseAdapter());
        FrameHierarchyController.setMainFrame(this); // Dialogs can be created with this as parent

        setUpWindow();
        createComponents();

        pack();
        setVisible(true);
    }

    /**
     * Set up window size and position
     */
    private void setUpWindow() {
        setPreferredSize(new Dimension(700, 500));
        setMinimumSize(new Dimension(600, 200));

        try {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int screenHeight = screenSize.height;
            int screenWidth = screenSize.width;
            setLocation(screenWidth / 2 - getPreferredSize().width / 2,
                    screenHeight / 2 - getPreferredSize().height / 2);
        } catch (Exception ignored) {

        }
    }

    /**
     * Creates all the components
     */
    private void createComponents() {
        Table table = new Table(this);
        SelectionController selectionController = new SelectionController(table.getSelectionModel());
        contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
        TableModel tableModel = (TableModel) table.getModel();

        Header header = new Header(this, tableModel, selectionController, table);
        contentPane.add(header, BorderLayout.NORTH);

        Footer footer = new Footer(table, selectionController);
        contentPane.add(footer, BorderLayout.SOUTH);
    }

    public void exit() {
        processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
