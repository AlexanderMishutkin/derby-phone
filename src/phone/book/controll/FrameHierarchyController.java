/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import javax.swing.*;

/**
 * Gives ability to easily get parent while dialog creation
 */
public class FrameHierarchyController {
    private static JFrame mainFrame = new JFrame(); // Stub! Should be set to the real main frame

    public static void setMainFrame(JFrame mainFrame) {
        FrameHierarchyController.mainFrame = mainFrame;
    }

    /**
     * Returns main frame of program. In current case - good variant for dialog parent
     *
     * @return main frame of program
     */
    public static JFrame getMainFrame() {
        return mainFrame;
    }
}
