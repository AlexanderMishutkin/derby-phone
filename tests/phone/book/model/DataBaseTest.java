/*
 * @author apmishutkin@edu.hse.ru
 */

package phone.book.model;

import org.apache.derby.iapi.services.io.FileUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing can take a lot of time for the first tine cause of driver loading
 *
 * Test DB is not always properly deleted due to bugs in Java native delete function
 * (author find it ideologically incorrect to load special library to delete directories!)
 *
 * However, test DB is always correctly cleared before each test not to spoil results
 */
class DataBaseTest {

    @BeforeEach
    void setUp() {
        FileUtil.removeDirectory(new File("Test"));
        DataBase.INSTANCE.DB_NAME = "Test";
        DataBase.INSTANCE.connectDB();

        try {
            if (!DataBase.INSTANCE.isNotConnected()) {
                DataBase.INSTANCE.connection.createStatement().execute("DROP TABLE Contacts");
                System.out.println("Old table removed");
                DataBase.INSTANCE.disconnect();
                DataBase.INSTANCE.connectDB();
            } else {
                System.out.println("There was no table");
            }
        } catch (SQLException exception) {
            fail(exception);
        }

        try {
            assertFalse(DataBase.INSTANCE.isNotConnected());
            DataBase.INSTANCE.addContact(new Contact(1, "12m34", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(2, "12s34", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(3, "12h34", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(4, "12m34", "k11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.addContact(new Contact(5, "12m345", "a11", "22p", "7", "0", "msc", new Date(), "kek"));
            DataBase.INSTANCE.getFilteredContacts("").forEach(System.out::println);
        } catch (SQLException | ContactValidateException exception) {
            fail(exception);
        }
    }

    @AfterEach
    void tearDown() {
        DataBase.INSTANCE.disconnect();
    }

    @AfterAll
    static void afterAll() {
        new File("Test").deleteOnExit();
    }

    @Test
    void isConnected() {
        final DataBase instance = DataBase.INSTANCE;
        try {
            assertFalse(instance.connection == null || instance.connection.isClosed());
            assertFalse(instance.isNotConnected());
        } catch (SQLException exception) {
            fail(exception);
        }
    }

    @Test
    void addContact() {
        try {
            assertEquals(0, DataBase.INSTANCE.getFilteredContacts("Z").size());
            DataBase.INSTANCE.addContact(new Contact(6, "ZZZ", "a3", "34", "7", "0", "msc", new Date(), "kek"));
            assertEquals(1, DataBase.INSTANCE.getFilteredContacts("Z").size());
        } catch (ContactValidateException | SQLException exception) {
            fail(exception);
        }
    }

    @Test
    void editContact() {
        try {
            Contact old = new Contact(1, "12m34", "a11", "22p", "7", "0", "msc", new Date(), "kek");
            assertEquals(0, DataBase.INSTANCE.getFilteredContacts("Z").size());
            DataBase.INSTANCE.editContact(new Contact(1, "ZZZ", "a3", "34", "7", "0", "msc", new Date(), "kek"));
            assertEquals(1, DataBase.INSTANCE.getFilteredContacts("Z").size());
            assertNull(DataBase.INSTANCE.getContactByName(old));
        } catch (ContactValidateException | SQLException exception) {
            fail(exception);
        }
    }

    @Test
    void getContactByName() {
        Contact c = new Contact(1, "12m34", "a11", "22p", "7", "0", "msc", new Date(), "kek");
        try {
            assertEquals(c, DataBase.INSTANCE.getContactByName(c));
            assertEquals(1, Objects.requireNonNull(DataBase.INSTANCE.getContactByName(c)).getId());
        } catch (Exception exception) {
            fail(exception);
        }
    }

    @Test
    void getFilteredContacts() {
        Contact c = new Contact(1, "12m34", "a11", "22p", "7", "0", "msc", new Date(), "kek");
        try {
            assertEquals(0, DataBase.INSTANCE.getFilteredContacts("Z").size());
            assertNotEquals(0, DataBase.INSTANCE.getFilteredContacts("1").size());
            assertTrue(DataBase.INSTANCE.getFilteredContacts("1").contains(c));
        } catch (ContactValidateException | SQLException exception) {
            fail(exception);
        }
    }

    @Test
    void isNotConnected() {
        assertFalse(DataBase.INSTANCE.isNotConnected());
        DataBase.INSTANCE.disconnect();
        assertTrue(DataBase.INSTANCE.isNotConnected());
    }

    @Test
    void deleteContact() {
        Contact c = new Contact(1, "12m34", "a11", "22p", "7", "0", "msc", new Date(), "kek");
        try {
            assertEquals(c, DataBase.INSTANCE.getContactByName(c));
            DataBase.INSTANCE.deleteContact(c);
            assertNull(DataBase.INSTANCE.getContactByName(c));
        } catch (Exception exception) {
            fail(exception);
        }
    }

    @Test
    void disconnect() {
        DataBase.INSTANCE.disconnect();
        assertTrue(DataBase.INSTANCE.isNotConnected());
    }

    @Test
    void clearTable() {
        try {
            DataBase.INSTANCE.clearTable();
            assertEquals(0, DataBase.INSTANCE.getFilteredContacts("").size());

            DataBase.INSTANCE.addContact(new Contact(1, "a", "a", "a", "9", "4", "f", "n"));
            assertEquals(1, DataBase.INSTANCE.getFilteredContacts("").size());
        } catch (SQLException | ContactValidateException e) {
            fail(e);
        }
    }
}