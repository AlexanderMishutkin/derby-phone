/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import phone.book.view.dialogs.MiscellaneousDialogs;
import phone.book.view.utils.MarginAndFonts;
import phone.book.view.dialogs.MergeDialog;

import javax.swing.table.AbstractTableModel;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static phone.book.model.MergeStrategy.*;

/**
 * Model for <code>PhoneBookTable</code>
 */
public class TableModel extends AbstractTableModel {
    //    private final List<Contact> contacts = new ArrayList<>(); // Now replaced by DB
    private final DataBase db = DataBase.INSTANCE;
    private final File source = new File("phone_book.csv");
    private String filter = "";
    private List<Contact> filteredContacts = null;
    private boolean storageExists;
    private MergeStrategy mergeStrategy;
    static private boolean silent = false;

    /**
     * @param silent never show dialogs?
     */
    protected static void setSilent(boolean silent) {
        TableModel.silent = silent;
    }

    public TableModel() {
        super();
        fireTableDataChanged();
        setMergeStrategy(MergeDialog::showMergeDialog);

        for (Contact contact :
                filteredContacts) {
            if (filteredContacts.stream().filter((Contact obj)->obj.equals(contact)).count() > 1) {
                if (isNotSilent()) MiscellaneousDialogs.showLoadFromDataBaseError();
                storageExists = false;
                return;
            }
        }
    }

    /**
     * Imports contacts from file
     *
     * @param src file with contacts
     * @throws IOException              if something wrong with file
     * @throws ContactValidateException if format of data in file is wrong
     */
    public void importTable(File src) throws IOException, ContactValidateException {
        filter = "";
        fireTableDataChanged();
        mergeContacts(CSV_Storage.load(src));
    }

    /**
     * Export contacts from table into .csv file
     *
     * @param dest file to export contacts
     * @throws IOException if something wrong with file
     */
    public void exportTable(File dest) throws IOException {
        try {
            CSV_Storage.save(dest, db.getFilteredContacts(""));
        } catch (ContactValidateException | SQLException e) {
            if (isNotSilent()) MiscellaneousDialogs.showLoadFromDataBaseError();
        }
    }

    /**
     * Export contacts from table into .csv file
     * <p>
     * Import only displayed contacts
     *
     * @param dest file to export contacts
     * @throws IOException if something wrong with file
     */
    public void exportFilteredTable(File dest) throws IOException {
        CSV_Storage.save(dest, filteredContacts);
    }

    /**
     * Export table data to default file
     *
     * @throws IOException if something wrong with file
     */
    public void save() throws IOException {
        try {
            CSV_Storage.save(source, db.getFilteredContacts(""));
        } catch (ContactValidateException e) {
            if (isNotSilent()) MiscellaneousDialogs.showLoadFromDataBaseError();
        } catch (SQLException throwable) {
            if (isNotSilent()) MiscellaneousDialogs.showSaveToDataBaseError();
        }
    }

    /**
     * Replace given contact
     *
     * @param newContact contact to replace with (need correct ID)
     * @throws IndexOutOfBoundsException if element is not in table
     */
    public void replaceContact(Contact newContact) throws IndexOutOfBoundsException {
        try {
            db.editContact(newContact);
            fireTableDataChanged();
        } catch (SQLException throwable) {
            if (isNotSilent()) MiscellaneousDialogs.showSaveToDataBaseError();
        }
    }

    /**
     * Removes contact from table
     *
     * @param contact to remove
     */
    public void removeContact(Contact contact) {
        int i = -1;
        if (filteredContacts.contains(contact)) {
            i = filteredContacts.indexOf(contact);
        }
        try {
            db.deleteContact(contact);
            if (i != -1) {
                fireFilterChanged();
                fireTableRowsDeleted(i, i);
            }
        } catch (SQLException throwable) {
            if (isNotSilent()) MiscellaneousDialogs.showSaveToDataBaseError();
        }
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return filteredContacts.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return Contact.getFieldsNames().length;
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return filteredContacts.get(rowIndex).getFields()[columnIndex];
    }

    /**
     * @param column column number
     * @return name of column
     */
    @Override
    public String getColumnName(int column) {
        return MarginAndFonts.headerToHTML(Contact.getFieldsNames()[column]);
    }

    /**
     * @return contacts with filter applied
     */
    public List<Contact> getFilteredContacts() {
        return filteredContacts;
    }

    /**
     * @param filter current filter as string
     */
    public void setFilter(String filter) {
        if (filter.equals(this.filter)) {
            return;
        }

        this.filter = filter;
        fireTableDataChanged();
    }

    /**
     * Update filtered list if filter changes
     */
    public void fireFilterChanged() {
        try {
            this.filteredContacts = db.getFilteredContacts(filter);
            storageExists = true;
        } catch (ContactValidateException | SQLException e) {
            if (isNotSilent()) MiscellaneousDialogs.showLoadFromDataBaseError();
            storageExists = false;
            if (this.filteredContacts == null) {
                this.filteredContacts = new ArrayList<>();
            }
        }
    }

    @Override
    public void fireTableDataChanged() {
        fireFilterChanged();
        super.fireTableDataChanged();
    }

    /**
     * @return filter - as query string
     */
    public String getFilter() {
        return filter;
    }

    /**
     * Was phone book loaded successfully in constructor
     *
     * @return true if phone book loaded successfully in constructor
     */
    public boolean isStorageExists() {
        return storageExists;
    }

    /**
     * Same as <code>List</code> contains
     */
    public boolean contains(Contact contact) throws ContactValidateException, SQLException {
        return db.getContactByName(contact) != null;
    }

    /**
     * Adds contacts to database
     *
     * @param newContacts contacts to add
     */
    public void addContacts(List<Contact> newContacts) {
        boolean twins = false;
        for (Contact newContact : newContacts) {
            try {
                twins = twins || contains(newContact);
            } catch (ContactValidateException | SQLException e) {
                if (isNotSilent()) MiscellaneousDialogs.showLoadFromDataBaseError();
                return;
            }
        }

        if (twins) {
            mergeContacts(newContacts);
        } else {
            try {
                for (Contact newContact : newContacts) {
                    db.addContact(newContact);
                }
            } catch (SQLException e) {
                if (isNotSilent()) MiscellaneousDialogs.showSaveToDataBaseError();
                return;
            }
        }
        fireTableDataChanged();
    }

    /**
     * Adds to table new contacts, checking for same contacts
     *
     * @param newContacts contacts to add in table
     */
    public void mergeContacts(List<Contact> newContacts) {
        int choice = 0;
        ArrayList<Contact> contacts;

        try {
            contacts = db.getFilteredContacts("");
        } catch (ContactValidateException | SQLException e) {
            if (isNotSilent()) MiscellaneousDialogs.showLoadFromDataBaseError();
            return;
        }

        for (Contact newContact : newContacts) {
            int j = contacts.indexOf(newContact);
            if (j >= 0) {
                if (choice == ALWAYS_LEAVE_OLD) {
                    continue;
                }
                if (choice == ALWAYS_TAKE_NEW) {
                    contacts.set(j, newContact);
                }


                ArrayList<Integer> diff = contacts.get(j).diff(newContact);
                if (diff != null && diff.size() > 0) {
                    choice = getMergeStrategy().chooseContact(contacts.get(j), newContact);

                    if (choice == LEAVE_OLD || choice == ALWAYS_LEAVE_OLD) {
                        continue;
                    }
                    if (choice == TAKE_NEW || choice == ALWAYS_TAKE_NEW) {
                        contacts.set(j, newContact);
                    }
                }
            } else {
                contacts.add(newContact);
            }
        }

        try {
            db.clearTable();
            for (Contact newContact : contacts) {
                db.addContact(newContact);
            }
        } catch (SQLException e) {
            if (isNotSilent()) MiscellaneousDialogs.showSaveToDataBaseError();
            return;
        }

        fireTableDataChanged();
    }

    protected MergeStrategy getMergeStrategy() {
        return mergeStrategy;
    }

    protected void setMergeStrategy(MergeStrategy mergeStrategy) {
        this.mergeStrategy = mergeStrategy;
    }

    static protected boolean isNotSilent() {
        return !silent;
    }

}
