/*
 * <a href="mailto:apmishutkin@edu.hse.ru"> Mishutkin Alexander </a>
 */

package phone.book.model;

import java.sql.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Singleton class that implements all the work with database
 */
public enum DataBase {
    /**
     * DataBase instance - use it everywhere
     */
    INSTANCE;

    final String DRIVER;
    final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    /**
     * Not final for testing needs
     */
    String DB_NAME;

    final String getConnectionURL() {
        return "jdbc:derby:" + DB_NAME + ";create=true";
    }

    Connection connection;

    {
        DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
        DB_NAME = "PhoneBook";
    }

    /**
     * @return whether connection to derby was successful
     */
    public boolean isNotConnected() {
        return connection == null;
    }

    /**
     * Tries to connect to derby DB
     */
    DataBase() {
        connectDB(); // Method is separated for testing
    }

    /**
     * Connects to DB
     * <p>
     * Separated for testing connection
     */
    void connectDB() {
        String createQuery = SQL_Queries.CREATE;

        try {
            connection = DriverManager.getConnection(getConnectionURL());
        } catch (SQLException throwable) {
            //throwable.printStackTrace();
            connection = null;
            return;
        }

        try {
            Statement statement = connection.createStatement();
            statement.execute(createQuery);
        } catch (SQLException throwable) {
            //throwable.printStackTrace();
        }

        try {
            createStatements();
        } catch (SQLException throwable) {
            //throwable.printStackTrace();
            try {
                connection.close();
            } catch (Exception ignored) {
                connection = null;
            }
        }
    }

    PreparedStatement add;
    PreparedStatement edit;
    PreparedStatement delete;
    PreparedStatement selectOne;
    PreparedStatement selectManyFiltered;

    /**
     * Prepares statements to use while editing contacts table
     *
     * @throws SQLException if connection was unsuccessful
     */
    private void createStatements() throws SQLException {
        if (isNotConnected()) {
            return;
        }
        add = connection.prepareStatement(SQL_Queries.ADD);
        edit = connection.prepareStatement(SQL_Queries.EDIT);
        delete = connection.prepareStatement(SQL_Queries.DELETE);
        selectOne = connection.prepareStatement(SQL_Queries.FIND);
        selectManyFiltered = connection.prepareStatement(SQL_Queries.FILTER);
    }

    /**
     * Adds contact to DB
     *
     * @param contact to add
     * @throws SQLException if DB is badly connected
     */
    public void addContact(Contact contact) throws SQLException {
        if (isNotConnected()) {
            return;
        }
        setArguments(contact, add);
        add.executeUpdate();
    }

    /**
     * Alter contact in DB
     *
     * @param contact to edit
     * @throws SQLException if DB is badly connected
     */
    public void editContact(Contact contact) throws SQLException {
        if (isNotConnected()) {
            return;
        }
        setArguments(contact, edit);
        edit.setObject(contact.getFields().length, contact.getId());
        edit.executeUpdate();
    }

    /**
     * Read a row in DB and create contact depending on row's data
     *
     * @param resultSet cursor on some row
     * @return contact
     * @throws SQLException if cursor is not on valid row
     * @throws ContactValidateException if data in the row is bad
     */
    private Contact contactFromCursor(ResultSet resultSet) throws SQLException, ContactValidateException {
        ArrayList<Object> row = new ArrayList<>();
        for (int i = 1; i <= Contact.getFieldsNames().length; i++) {
            row.add(resultSet.getObject(i));
        }
        if (row.get(Contact.BIRTHDAY_INDEX) == null) {
            row.set(Contact.BIRTHDAY_INDEX, " ");
        } else {
            row.set(Contact.BIRTHDAY_INDEX, FORMATTER.format(((Date) row.get(Contact.BIRTHDAY_INDEX)).toLocalDate()));
        }
        return Contact.of(row.toArray());
    }

    /**
     * Searches fr given contact in DB
     *
     * @param contact to search for
     * @return contact with the same name or <code>null</code>
     * @throws SQLException if DB is badly connected
     * @throws ContactValidateException if data in DB is bad
     */
    public Contact getContactByName(Contact contact) throws SQLException, ContactValidateException {
        if (isNotConnected()) {
            throw new SQLException("Database is not connected");
        }

        selectOne.setObject(1, contact.getSurname());
        selectOne.setObject(2, contact.getName());
        selectOne.setObject(3, contact.getPatronymic());
        final ResultSet resultSet = selectOne.executeQuery();
        if (resultSet.next()) {
            return contactFromCursor(resultSet);
        } else {
            return null;
        }
    }

    /**
     * Return contacts that satisfy filter
     *
     * @param filter to search with
     * @return list of contacts
     * @throws ContactValidateException if data in DB is corrupted
     * @throws SQLException if DB is badly connected
     */
    public ArrayList<Contact> getFilteredContacts(String filter) throws ContactValidateException, SQLException {
        if (isNotConnected()) {
            throw new SQLException("Database is not connected");
        }

        selectManyFiltered.setObject(1, filter);
        selectManyFiltered.setObject(2, filter);
        selectManyFiltered.setObject(3, filter);
        final ResultSet resultSet = selectManyFiltered.executeQuery();
        ArrayList<Contact> answer = new ArrayList<>();
        while (resultSet.next()) {
            answer.add(contactFromCursor(resultSet));
        }

        return answer;
    }

    /**
     * @param contact to get argument's values from
     * @param statement any statement with arguments to enter from contact fields
     * @throws SQLException if DB is badly connected
     */
    private void setArguments(Contact contact, PreparedStatement statement) throws SQLException {
        if (isNotConnected()) {
            return;
        }
        for (int i = 1; i < contact.getFields().length; i++) {
            if (i == Contact.BIRTHDAY_INDEX && contact.getFields()[i] == "") {
                statement.setObject(i, null);
                continue;
            }
            statement.setObject(i, contact.getFields()[i]);
        }
    }

    /**
     * Removes Contact from DB
     *
     * @param contact to delete
     * @throws SQLException if DB is badly connected
     */
    public void deleteContact(Contact contact) throws SQLException {
        if (isNotConnected()) {
            throw new SQLException("Database is not connected");
        }

        delete.setObject(1, contact.getId());
        delete.executeUpdate();
    }

    /**
     * Removes all contacts in DB
     *
     * @throws SQLException if DB is badly connected
     */
    public void clearTable() throws SQLException {
        connection.createStatement().execute(SQL_Queries.CLEAR);
    }

    /**
     * Finalize was made final in enum (((((
     */
    public void disconnect() {
        if (isNotConnected()) {
            return;
        }
        try {
            connection.close();
        } catch (SQLException ignored) {
            System.out.println("connection close problems");
        }
        connection = null;
        add = null;
        edit = null;
        delete = null;
        selectOne = null;
        selectManyFiltered = null;
    }

    /**
     * Tries to fix DB load error by dropping or clearing table, if it is possible
     * Be careful! Removes all data in DB!
     */
    public void reset() {
        try {
            if (!isNotConnected()) {
                disconnect();
            }
            connectDB();
            try {
                clearTable();
            } catch (Exception ignored) {
            }
            try {
                connection.createStatement().execute("DROP TABLE Contacts");
            } catch (Exception ignored) {
            }
            disconnect();
            connectDB();
        } catch (Exception ignored) {

        }
    }
}
